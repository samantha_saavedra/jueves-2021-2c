using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    [SerializeField] float angleY;
    [SerializeField] Item item;
    [SerializeField] float rotationSpeed;
    [SerializeField] float angleMin;
    [SerializeField] float angleMax;

    private Player player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void Update()
    {
        if (player.DeliveredItem.Contains(item))
        {
            transform.Rotate(Vector3.up * angleY, rotationSpeed * Time.deltaTime);
            Vector3 currentRotation = transform.localRotation.eulerAngles;
            currentRotation.y = Mathf.Clamp(currentRotation.y, angleMin, angleMax);
            //transform.localEulerAngles = currentRotation;
            transform.localRotation = Quaternion.Euler(currentRotation);
        }
    }
}
