using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClueTriggerController : MonoBehaviour
{
    [SerializeField] GameObject clueMusic;
    
    private void OnTriggerEnter(Collider other)
    {
        clueMusic.SetActive(true);
        Destroy(this.gameObject);
    }
}
