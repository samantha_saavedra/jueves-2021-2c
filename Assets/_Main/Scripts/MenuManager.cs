using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private string Level1;
    public GameObject eyes;
    public AudioSource evilLaugh;
    public GameObject audioSource;

    public void StartButton()
    {
        eyes.SetActive(true);
        audioSource.SetActive(true);
        evilLaugh.Play();
        SceneManager.LoadScene(Level1);
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
