using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] string itemName;
    [SerializeField] Sprite itemIcon;
    [SerializeField] GameObject clueMusic;

    GameManager gm;

    private void Start()
    {
        this.gameObject.SetActive(false);
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    public Item(string itemName)
    {
        this.itemName = itemName;
    }

    public string ItemName { get => itemName; }

    private void OnTriggerStay(Collider other)
    {
        Player player = other.GetComponent<Player>();
        if (player == null) player = other.GetComponentInParent<Player>();
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (player != null) player.CollectingItem(this);
            gm.itemFrame.SetActive(true);
            gm.item.sprite = itemIcon;
            clueMusic.SetActive(false);
            Debug.Log("The items was picked!");
            this.gameObject.SetActive(false);
        }
    }
}
