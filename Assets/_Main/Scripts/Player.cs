using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private List<Item> collectedItems;
    private List<Item> deliveredItem;

    public List<Item> CollectedItems { get => collectedItems; }
    public List<Item> DeliveredItem { get => deliveredItem; }

    private void Start()
    {
        collectedItems = new List<Item>();
        deliveredItem = new List<Item>();
    }

    public void CollectingItem(Item item)
    {
        collectedItems.Add(item);
    }

    public void DeliveringItem(Item item)
    {
        deliveredItem.Add(item);
        collectedItems.Remove(item);
    }
}
