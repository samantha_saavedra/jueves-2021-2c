using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{
    [SerializeField] string onEnterText;
    [SerializeField] string onCompletionText;
    [SerializeField] GameObject itemObject;

    GameManager gm;

    Item item;
    Player player;

    string itemName;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        item = itemObject.GetComponent<Item>();
        itemName = item.ItemName;
    }

    private void OnTriggerStay(Collider other)
    {
        player = other.GetComponent<Player>();
        if (player == null) player = other.GetComponentInParent<Player>();
        if (player != null)
        {
            if (!player.CollectedItems.Contains(item) && !player.DeliveredItem.Contains(item) && Input.GetKeyDown(KeyCode.E))
            {
                itemObject.SetActive(true);
                gm.textFrame.SetActive(true);
                gm.text.text = onEnterText.Replace("#ITEM#", itemName);
                //Debug.Log(onEnterText.Replace("#ITEM#", itemName));
            }
            if (player.CollectedItems.Contains(item) && !player.DeliveredItem.Contains(item) && Input.GetKeyDown(KeyCode.E))
            {
                player.DeliveringItem(item);
                gm.textFrame.SetActive(true);
                gm.itemFrame.SetActive(false);
                gm.text.text = onCompletionText;
                //Debug.Log(onCompletionText);
            }
            if (!player.CollectedItems.Contains(item) && player.DeliveredItem.Contains(item) && Input.GetKeyDown(KeyCode.E))
            {
                gm.textFrame.SetActive(true);
                gm.text.text = onCompletionText;
                //Debug.Log(onCompletionText);
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                gm.textFrame.SetActive(false);
            }
        }
        //Debug.Log($"{other.gameObject.name}");
    }
}
