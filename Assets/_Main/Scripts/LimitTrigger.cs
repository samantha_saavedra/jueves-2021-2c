using UnityEngine;

public class LimitTrigger : MonoBehaviour
{
    [SerializeField] Item item;

    private Player player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void Update()
    {
        if (player.DeliveredItem.Contains(item))
        {
            Destroy(this.gameObject);
        }
    }
}
